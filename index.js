const electron = require('electron')
const path = require('path')
const walk = require('walk')

const usbDir = "/mnt"


let videoList = []
let vidID = 0;

const { app, BrowserWindow, Menu, ipcMain} = electron

app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');

let mainWindow

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width: 1024,
        height: 565,
        minWidth: 1024,
        minHeight: 565,
    })
    mainWindow.loadURL(`file://${__dirname}/main.html`)
    mainWindow.on('closed', () => app.quit())
})

ipcMain.on('video:ready', (event, ready) => {
    fileWalker()
})

ipcMain.on('video:load', (event, id) => {
    let video = videoList[id].path
    mainWindow.webContents.send('todo:add', video)
})

fileWalker = () => {
    const walker = walk.walk(usbDir, { followLinks: false })
    walker.on('file', function(root, stat, next) {
        const re = /(?:\.([^.]+))?$/
        const ext = re.exec(stat.name)[1]

        if ((ext == "mp4") || (ext == "mkv") || (ext == "mov") || (ext == "avi")) {
            videoList.push({
                id: vidID,
                file: stat.name,
                path: root + '/' + stat.name
            })
            vidID++
        }

        next()
    })
    
    walker.on('end', function() {
        mainWindow.webContents.send('video:renderlist', videoList)
    })
}

